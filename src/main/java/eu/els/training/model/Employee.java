package eu.els.training.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class Employee {

    private String id;
    private String name;
    private String designation;
    private double salary;

}
