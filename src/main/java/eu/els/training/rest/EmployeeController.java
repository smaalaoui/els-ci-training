package eu.els.training.rest;

import eu.els.training.model.Employee;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static eu.els.training.utils.EmployeeUtils.random;

@RequestMapping("/api/v1/employees")
public class EmployeeController {

    private List<Employee> employees = random();

    @GetMapping
    public ResponseEntity<List<Employee>> employees() {
        return new ResponseEntity<>(employees, new HttpHeaders(), HttpStatus.OK);
    }

}
