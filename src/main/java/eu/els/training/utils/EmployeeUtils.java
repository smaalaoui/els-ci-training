package eu.els.training.utils;

import eu.els.training.model.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.UUID.randomUUID;

public class EmployeeUtils {

    private EmployeeUtils() {
        super();
    }

    public static List<Employee> random() {
        List<Employee> tempEmployees = new ArrayList<>();
        Employee emp1 = new Employee();
        emp1.setName("emp1");
        emp1.setDesignation("manager");
        emp1.setId(randomUUID().toString());
        emp1.setSalary(3000);
        Employee emp2 = new Employee();
        emp2.setName("emp2");
        emp2.setDesignation("developer");
        emp2.setId(UUID.randomUUID().toString());
        emp2.setSalary(3000);
        tempEmployees.add(emp1);
        tempEmployees.add(emp2);
        return tempEmployees;
    }

}
