package eu.els.training.rest;

import eu.els.training.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class EmployeeControllerTest {

    private MockMvc mvc;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.standaloneSetup(new EmployeeController())
                .build();
    }

    @Test
    public void GIVEN_bad_url_WHEN_get_employees_THEN_should_return_http_status_404() throws Exception {
        // GIVEN


        // WHEN
        ResultActions resultActions = mvc.perform(get("/api/v1/employee__").contentType(MediaType.APPLICATION_JSON));

        // THEN
        resultActions
                .andExpect(status().isNotFound());
    }

    @Test
    public void GIVEN_correct_url_WHEN_get_employees_THEN_should_return_list_of_employees_with_given_values() throws Exception {
        // GIVEN


        // WHEN
        ResultActions resultActions = mvc.perform(get("/api/v1/employees").contentType(MediaType.APPLICATION_JSON));

        // THEN
        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").exists())
                .andExpect(jsonPath("$[0].name").value("emp1"))
                .andExpect(jsonPath("$[0].designation").value("manager"))
                .andExpect(jsonPath("$[0].salary").value("3000.0"))

                .andExpect(jsonPath("$[1].id").exists())
                .andExpect(jsonPath("$[1].name").value("emp2"))
                .andExpect(jsonPath("$[1].designation").value("developer"))
                .andExpect(jsonPath("$[1].salary").value("3000.0"));
    }

}
